module.exports = {
  mode : 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme=>({
        'banniere': "url('/Assets/photo-sable.jpg')",
        'image' : "url('/Assets/arbre.jpg')",
        'imagedeux' : "url('/Assets/city.jpg')",
        'carte' : "url('/Assets/carte.jpg')",
        'bannieredeux' : "url('/Assets/citrouille.jpg')",
        'imagetrois' : "url('/Assets/desert.jpg')",
        'paysage' : "url('/Assets/paysage.jpg')",
        'people' : "url('/Assets/people.jpg')",
        'world' :"url('/Assets/world.jpg')",
        'nature':"url('/Assets/nature.jpg')",
        'service':"url('/Assets/sunset.jpg')",
        'servicedeux':"url('/Assets/servicedeux.jpg')",
      })
    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
}