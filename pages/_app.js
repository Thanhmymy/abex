import '../styles/globals.css'
import '../styles/Nav.css'
import 'react-toastify/dist/ReactToastify.min.css';
import Layout from '../components/Layout';

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>

  )
}

export default MyApp
