import Footer from "../components/Footer";
import Nav from "../components/Nav";

const NotFound = ()=>{
return(
    <div className="flex flex-col min-h-screen">
        <Nav/>

        <div className="flex-1">
            <h1 className="text-4xl text-center">404 Not Found</h1>
            <p className="text-center mt-3">Veuillez vous redirigé vers l'accueil. </p>
        </div>

        <Footer/>
    </div>
)
}

export default NotFound;