
import Footer from "../../components/Footer";
import Nav from "../../components/Nav";
import { sanityClient, urlFor } from "../../sanity";


function Post({ post }) {

    return (
        <>
            <Nav />
            <div className="max-w-6xl mx-auto mb-5">

                <h1 className='text-center text-3xl font-bold mb-5 mt-5'>{post.title}</h1>
                <div className="flex justify-center">
                    <img className="mb-5" src={urlFor(post.mainImage).url()} alt="" />
                </div>
                <div className="flex mt-5 mb-5 items-center">
                    <div className="flex ">
                        <img className="h-20 w-24 rounded-full" src={urlFor(post.author.image).url()} alt="" />
                    </div>
                    <div>
                        <p className="font-extralight text-sm ml-5"><span className="text-green-500">{post.author.name}</span> - Published at {new Date(post._createdAt).toLocaleString()}</p></div>

                </div>
                <p className="mb-5 italic font-medium text-lg">{post.description}</p>
                <p className="text-lg font-serif">{post.firsttext}</p>
            </div>
            <Footer />
        </>

    )
}


export default Post;
export const getStaticPaths = async () => {
    const query = `
    *[_type == "blog"]{
        _id,
        slug{
        current
      }
      }
    `;

    const posts = await sanityClient.fetch(query);

    const paths = posts.map((post) => ({
        params: {
            slug: post.slug.current
        },
    }));
    return {
        paths,
        fallback: 'blocking'
    };
};

export const getStaticProps = async ({ params }) => {
    const query = `
    *[_type == "blog" && slug.current == $slug][0]{
        _id,
        _createdAt,
        title,
        author->{
        name,
        image
      },
      'comments': *[
        _type == "comment" &&
        post._ref == ^._id &&
        approved == true],
      description,
      mainImage,
      slug,
      body,
      firsttext,
      }
 `

    const post = await sanityClient.fetch(query, {
        slug: params?.slug,
    });
    if (!post) {
        return {
            notFound: true
        }


    }
    return {
        props: {
            post,
        },
        revalidate: 60,
    }
}