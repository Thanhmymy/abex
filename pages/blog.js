import PortableText from "react-portable-text";
import Nav from "../components/Nav";
import { sanityClient, urlFor } from "../sanity";
import Link from 'next/link'
import Footer from "../components/Footer";
function blog({ posts }) {

    return (
        <>
            <Nav />
            <h1 className="text-center text-4xl font-bold mt-5 mb-5">Blog</h1>
            <div className="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-2 gap-x-4 m-5">
                {posts.map(post => (
                    <Link key={post._id} href={`/post/${post.slug.current}`}>

                        <div className="border p-2 rounded  flex flex-col mt-5 cursor-pointer hover:border hover:border-black">
                            <div className="flex flex-col items-center">
                                <p className="text-xl font-semibold mb-5">{post.title}</p>
                                <img className="h-36 object-cover w-full " src={urlFor(post.mainImage).url()} alt="" />


                                <p className="mb-5 mt-5 line-clamp-3">{post.description}</p>
                            </div>
                            <div className="flex mt-2 flex-row-reverse items-center justify-between">
                                <div className="flex flex-col items-center">
                                    <img className="h-10 w-12 rounded-full" src={urlFor(post.author.image).url()} alt="" />
                                </div>
                                <div>                    
                                    <p className="font-extralight text-sm"> Blog post by <span className="text-green-500">{post.author.name}</span> - Published at {new Date(post._createdAt).toLocaleString()}</p></div>

                            </div>

                            {/* <PortableText
                        dataset={process.env.NEXT_PUBLIC_SANITY_DATASET}
                        projectId={process.env.NEXT_PUBLIC_SANITY_PROJECT_ID}
                        content={post.body}
                        serializers={{
                            p: ({ children }) => (
                                <p className="mt-5">{children}</p>
                            )
                        }}
                    /> */}

                        </div>
                    </Link>
                ))}
            </div>
            <Footer />
        </>

    )
}

export default blog;

export const getServerSideProps = async () => {
    const query = `
    *[_type == "blog"]{
      _id,
      title,
      _createdAt,
      author-> {
      name,
      image
    },
    description,
    mainImage,
    slug,
    body
    }
    `;

    const posts = await sanityClient.fetch(query)
    return {
        props: {
            posts,
        }
    }
}