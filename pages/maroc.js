import { LocationMarkerIcon, MailIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/solid";
import Footer from "../components/Footer";
import Formulaire from "../components/Formulaire";
import Nav from "../components/Nav";


function maroc() {
    return <div>
        <Nav />
        <div className="bg-imagedeux lg:p-64 text-shadow p-20 bg-center bg-cover">
            <div className="text-center">
                <h1 className="text-[#F4B40A] font-bold text-3xl text-shadow-md">LE MAROC : UN "HUB" STRATEGIQUE</h1>
                <p className="text-white font-bold text-xl mb-5">De la recherche de nouveaux débouchés à une stratégie globale d'internationalisation, le Maroc vous offre de nombreuses avantages</p>
                <a href="https://calendly.com/abex/consultation?month=2022-04" target="_blank" className="bg-[#204683] p-2 mt-3 text-[#F4B40A] font-bold rounded-md hover:bg-[#436aaa] md:uppercase md:text-2xl lg:uppercase lg:text-2xl">Prendre Rendez-vous</a>
            </div>

        </div>
        <div className="flex justify-center mt-16 m-5">
            <div className="grid lg:grid-cols-2 md:grid-cols-1 lg:gap-x-16 lg:gap-y-10 gap-y-10">
                <div className="bg-nature bg-center bg-cover p-14 lg:p-32 md:p-16">
                    <div className="bg-[#00000083] p-5">
                        <p className="text-[#f4b40a] text-shadow font-bold">Situation géographique</p>
                        <p className="text-white">De part sa position géographique, entre
                            l’Europe et l’Afrique, le Maroc, est une
                            véritable passerelle pour votre
                            développement sur le reste du continent.

                            Ses ouvertures sur l’Atlantique et la
                            Méditerranée vous donne accès à de
                            nombreux marchés, vers l’Amérique, l’Europe
                            et le Proche Orient</p>
                    </div>

                </div>
                <div className="bg-nature bg-center bg-cover p-14 lg:p-32 md:p-16">
                    <div className="bg-[#00000083]  p-5">
                        <p className="text-[#f4b40a] text-shadow font-bold">ENVIRONNEMENT DES AFFAIRES </p>

                        <p className="text-white">Avec une croissance soutenue et une inflation
                            maitrisé depuis plusieurs années, le Maroc est
                            une destination privilégiée des investisseurs
                            étrangers.

                            Les ambitieuses stratégies sectorielles du
                            gouvernement les nombreux accords de libres
                            échanges vous permettront un développement
                            optimal de votre activité à l’international.</p>
                    </div>

                </div>
                <div className="bg-nature bg-center bg-cover p-14 lg:p-32 md:p-16">
                    <div className="bg-[#00000083] p-5">
                        <p className="text-[#f4b40a] text-shadow font-bold">INFRASTRUCTURE MODERNE</p>
                        <p className="text-white">Le Maroc est le 1er pays d’Afrique en terme
                            d’infrastucture avec un solide réseau de
                            transports très diversifiés.

                            - Nombreux aéroports internationaux et
                            nationaux

                            - 1er port d’Afrique et de Méditéranée avec
                            Tanger Med

                            - Dense réseau autoroutier, routier et
                            ferroviaire

                            - Réseau de télécommunication de très haut</p>
                    </div>

                </div>
                <div className="bg-nature bg-center bg-cover p-14 lg:p-32 md:p-16">
                    <div className="bg-[#00000083] p-5">
                        <p className="text-[#f4b40a] text-shadow font-bold">MAIN D’ŒUVRE QUALIFIE ET COMPETITIVE</p>
                        <p className="text-white">Optimiser vos coûts et gagner en compétitivités
                            face à vos concurrents internationaux grâce à
                            une main d’œuvre toujours plus qualifiée et
                            hyper compétitive dans de nombreux secteurs
                            d’activités.

                            La maitrise de la langue française par une
                            grande partie de la population facilitera votre
                            immersion sur le marché local.</p>
                    </div>
                </div>
            </div>

        </div>

        {/* Section deux */}
        <div className="lg:flex mt-5 p-5">
            <div className="bg-[#204683] p-5 flex flex-col text-lg text-white lg:2/5">
                <h2 className="text-[#f4b40a] text-2xl font-bold mb-4">Nous vous accompagnons à tirer
                    pleinement profit des fortes
                    potentialités du Maroc</h2>
                <div className="lg:mb-60">
                    <p>
                        Outre le fort potentiel du marché
                        marocains porté par de forte
                        stratégie de développement
                        sectorielle, le Maroc vous offre un
                        cadre optimal pour votre croissance
                        à l’international grâce à de
                        nombreux avantages compétitif.
                    </p>
                    <ul>
                        <li className="list-disc ml-6">Proximité géographique avec l’Europe</li>
                        <li className="list-disc ml-6">Fiscalité avantageuse</li>
                        <li className="list-disc ml-6">Ouverture internationale</li>
                        <li className="list-disc ml-6">Matière première accessible</li>
                        <li className="list-disc ml-6">Ressources humaines qualifiés et compétitif etc…</li>
                        <li className="list-disc ml-6">Infrastructure moderne</li>
                        <li className="list-disc ml-6">Environnement politique et economique stable</li>
                    </ul>














                </div>



            </div>
            <img className="lg:w-3/5" src="/Assets/egypt.jpg" alt="" />
        </div>
        <br />
        <Formulaire/>
        {/* Footer*/}
        <Footer/>

    </div>
}

export default maroc;