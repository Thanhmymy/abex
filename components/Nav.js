import React, { useState } from 'react'
import Link from 'next/link'

function Nav() {
    const [showLinks, setShowLinks] = useState(false);
    const handleShowLinks = () => {
        setShowLinks(!showLinks)
    }
    return <div>
        <nav className={`navbar ${showLinks ? "show-nav" : "hide-nav"} `}>
            <div className="navbar_logo"><a href="/">
                <img className="h-14" src="/Assets/Abex.png" alt="" />
            </a></div>
            <ul className="navbar_links">
                <a className="navbar_link" href="/services">
                    <li className="navbar_item">
                        Missions et Services
                    </li>
                </a>
                <a className="navbar_link" href="/externalisation">
                    <li className="navbar_item">
                        Externalisation Export
                    </li>
                </a>


                <a className="navbar_link" href="/maroc">
                    <li className="navbar_item ">
                        Maroc
                    </li>
                </a>

                <a className="navbar_link" href="/blog">
                    <li className="navbar_item ">
                        Blog
                    </li>
                </a>
                <a className="navbar_link" href="/contact">
                    <li className="navbar_item">
                        Contact</li>
                </a>
            </ul>
            <button className="navbar_burger" onClick={handleShowLinks}>
                <span className="burger-bar"></span>
            </button>
        </nav>
    </div>
}

export default Nav
