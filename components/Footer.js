import { LocationMarkerIcon, MailIcon, PhoneIcon, QuestionMarkCircleIcon } from '@heroicons/react/solid'
import Link from 'next/link'
import ReactCountryFlag from "react-country-flag"
function Footer() {
  return (
    <footer className=" bg-gray-100">
      <div className="border-t lg:flex justify-around items-center p-5">
        <div className="items-center flex flex-col">
          <Link href="/">
            <img className="mb-2 h-32 cursor-pointer" src="/Assets/Abex.png" alt="" />
          </Link>

          <p className="text-center mb-5">Votre partenaire commercial en Afrique</p>


        </div>

        <div className="flex flex-col items-center mb-5">
          <div className="flex flex-col items-center space-y-2">
            <ReactCountryFlag countryCode="ma" svg style={{
              width: '2em',
              height: '2em',
            }} />
            <p>214, Bd Ibnou Sina 20210 </p>
            <p>Casablanca Maroc</p>
          </div>
          <ReactCountryFlag countryCode="fr" svg style={{
            width: '2em',
            height: '2em',
          }} />
          <p>154 rue de Rome, 13006 Marseille</p>
        </div>

        <div className="flex flex-col items-center mb-5">
          <PhoneIcon className="h-8 text-gray-800" />
          <Link href="tel:0667620604">
            <p className="cursor-pointer hover:underline hover:text-blue-700">+33 6 67 62 06 04</p>
          </Link>

        </div>
        <div className="flex flex-col items-center mb-5">
          <MailIcon className="h-8 text-gray-800" />
          <Link href="mailto:contact@abex-export.com">
            <p className="cursor-pointer hover:underline hover:text-blue-700">contact@abex-export.com</p>
          </Link>

        </div>
      </div>

      <p className=" text-center mb-5">©Copyright African Bussiness Export-2022</p>
    </footer>
  )
}

export default Footer;