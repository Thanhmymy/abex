import { ToastContainer, toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import emailjs from '@emailjs/browser';
import { useState } from 'react';
import ReactCountryFlag from "react-country-flag"
import Link from 'next/link';

function Formulaire() {
    const [submitted, setSubmitted] = useState(false)
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors },
    } = useForm();


    const toastifySuccess = () => {
        toast('Message envoyé', {
            position: 'bottom-right',
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false,
            className: 'submit-feedback success',
            toastId: 'notifyToast'
        });
    };

    const onSubmit = async (data) => {
        const { name, email, subject, message } = data;
        try {
            const templateParams = {
                name,
                email,
                subject,
                message
            };
            await emailjs.send(
                process.env.NEXT_PUBLIC_SERVICE_ID,
                process.env.NEXT_PUBLIC_TEMPLATE_ID,
                templateParams,
                process.env.NEXT_PUBLIC_USER_ID
            );

            reset();
            setSubmitted(true)
            toastifySuccess()
        } catch (e) {
            console.log(e);
        }

    }
    return (
        <div className="max-w-4xl mx-auto">
            {/* <div className="p-5">

                <p className="text-[#204683] text-lg mb-5 text-center">Laissez-nous vos coordonnées, nous vous recontacterons pour échanger sur vos projets</p>
                <div className="mx-auto max-w-4xl">
                    <form className="flex flex-col mx-auto lg:w-4/5 mt-10 " onSubmit={handleSubmit(onSubmit)}>
                            <label className="mt-5">Prénom et Nom:</label>
                            <input className="border border-black p-3" placeholder="Nom et Prénom*" type="text" {...register('name', {
                                required: { value: true, message: 'Please enter your name' },
                                maxLength: {
                                    value: 30,
                                    message: 'Please use 30 characters or less'
                                }
                            })} />
                            {errors.name && <span className='errorMessage'>{errors.name.message}</span>}
                            <label className="mt-5">Téléphone:</label>
                            <input className="border border-black p-3" placeholder="Téléphone" type="text"
                                {...register('telephone', {
                                    required: true
                                })} />
                            {errors.telephone && <span className='errorMessage'>{errors.telephone.message}</span>}

                        <label className="mt-5">Email:</label>
                        <input className="border border-black p-3" placeholder="Adresse e-mail*" type="email"
                            {...register('email', {
                                required: true,
                                pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
                            })} />
                        {errors.email && (
                            <span className='errorMessage'>Please enter a valid email address</span>
                        )}
                        <label className="mt-5">Message:</label>
                        <textarea className="border border-black p-3" placeholder="Message(facultatif)" name="Message" id="message" cols="30" rows="5"
                            {...register('message', {
                                required: true
                            })}></textarea>
                        {errors.message && <span className='errorMessage'>Please enter a message</span>}
                        <button className="bg-[#204683] p-2 w-1/2 text-white text-lg mt-5">ENVOYER</button>
                    </form>
                    <ToastContainer/>
                </div>
            </div> */}
            <div className="text-center mt-5">
                <h1 className='mb-8 text-3xl font-bold text-center' >Prendre un rendez-vous ?</h1>
                <a href="https://calendly.com/abex/consultation?month=2022-04" target="_blank" className="bg-[#204683] p-2  text-[#F4B40A] font-bold rounded-md hover:bg-[#436aaa] uppercase text-2xl">Prendre Rendez-vous</a>
            </div>

            <h1 className="text-3xl font-bold text-center mt-10">Contactez-nous</h1>

            <div className="p-2 lg:flex lg:justify-around mt-7 mb-5 items-center">
                <div className="lg:w-1/2">


                    {submitted ? (
                        <div className="text-center flex flex-col items-center border p-2 border-black">

                            <p>Nous avons bien receptionné votre message. </p>
                            <p>Merci, à bientot !</p>
                        </div>

                    ) : (<>
                        <h2 className="text-center font-medium text-xl lg:mt-16">Ecrivez-nous</h2>
                        <form className="lg:p-3 p-4" id='contact-form' onSubmit={handleSubmit(onSubmit)}>
                            <div className="lg:flex">
                                <input className="border lg:mr-5 border-black p-3 mb-5 w-full"
                                    placeholder="Nom/Prénom*"
                                    name="name"
                                    type="text"
                                    {...register('name', {
                                        required: { value: true, message: 'Please enter your name' },
                                        maxLength: {
                                            value: 30,
                                            message: 'Please use 30 characters or less'
                                        }
                                    })} />
                                {errors.name && <span className='errorMessage'>{errors.name.message}</span>}
                                <input className="border border-black p-3 mb-5 w-full"
                                    placeholder="Téléphone"
                                    type="text"{...register('telephone', {
                                        required: true
                                    })} />
                                {errors.telephone && <span className='errorMessage'>{errors.telephone.message}</span>}
                            </div>

                            <input className="border border-black p-3 mb-5 w-full"
                                placeholder="Adresse e-mail*"
                                type="email"
                                name='email'
                                {...register('email', {
                                    required: true,
                                    pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
                                })} />
                            {errors.email && (
                                <span className='errorMessage'>Please enter a valid email address</span>
                            )}
                            <textarea className="border border-black p-3 mb-5 w-full"
                                placeholder="Message"
                                type="text"
                                name='message'
                                cols="30" rows="5"
                                {...register('message', {
                                    required: true
                                })} />
                            {errors.message && <span className='errorMessage'>Please enter a message</span>}
                            <button type="submit" className="bg-[#204683] p-3 text-white">Envoyer</button>
                        </form>
                    </>

                    )}

                    <ToastContainer />
                </div>

                <div className="lg:w-1/2 lg:p-5 text-center space-y-2 flex flex-col justify-center items-center">
                    <h2 className="mb-5 font-bold text-xl">Contact</h2>
                    <ReactCountryFlag countryCode="fr" svg style={{
                        width: '2em',
                        height: '2em',
                    }} />
                    <p>154 rue de Rome, 13006 Marseille</p>
                    <ReactCountryFlag countryCode="ma" svg style={{
                        width: '2em',
                        height: '2em',
                    }} />
                    <p>214, Bd Ibnou Sina 20210 </p>
                    <p>Casablanca Maroc</p>
                    <p>Contact direct : Mr Hocine.H</p>
                    <Link href="tel:0667620604">
                        <p className="cursor-pointer hover:underline hover:text-blue-700">+33 6 67 62 06 04</p>
                    </Link>
                    <Link href="mailto:contact@abex-export.com">
                    <p className="cursor-pointer hover:underline hover:text-blue-700">contact@abex-export.com</p>
                    </Link>


                    <div className="flex flex-col items-center">
                        <h2 className="mt-5 font-bold text-xl">Reseaux Sociaux</h2>
                        <div className="flex mt-3" >
                            <a href="https://www.linkedin.com/in/hhorri/"><img className="h-14 p-1" src="Assets/linkedin.png" alt="" /></a>
                        </div>
                    </div>


                </div>


            </div>
        </div>
    )
}

export default Formulaire;