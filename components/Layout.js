import CookieConsent from "react-cookie-consent";

function Layout({ children }) {
    return (
        <>
            {children}
            <CookieConsent
                location="bottom"
                buttonText="J'accepte"
                enableDeclineButton={true}
                declineButtonText={'Je refuse'}
                cookieName="Cookie Abex"
                style={{ background: "#2B373B" }}
                buttonStyle={{ color: "#4e503b", fontSize: "13px" }}
                expires={150}
            >
                Ce site utilise des cookies pour améliorer votre expérience.{" "}
            </CookieConsent>
        </>

    )

}
export default Layout;