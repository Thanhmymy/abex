import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { useRouter } from 'next/router';
import Link from 'next/link'


function Whatsapp(){
    const router = useRouter();
return(
    <Link href='https://wa.me/33667620604'>
    <a title='Whatsapp Abex' 
    className={`fixed z-40 w-16 h-16 p-2 rounded-full bottom-4 right-4 lg:right-6 border border-black bg-green-400  hover:border-green-400  
    flex justify-center items-center hover:scale-110 transition transform duration-100 ease-out cursor-pointer 
    ${router.pathname === '/diagnostic' && 'hidden'}`}
    target="_blank" rel="noreferrer">
      <span className="absolute top-0 right-0 rounded-full h-3 w-3 bg-green-400 border"></span>
      <span className="absolute top-0 right-0 animate-ping rounded-full h-3 w-3 bg-green-400 "></span>
      <FontAwesomeIcon className="text-white h-12" icon={faWhatsapp} />
    </a>
</Link>
)
}

export default Whatsapp;